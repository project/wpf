<?php

namespace Drupal\wpf;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Image\ImageInterface;

/**
 * Fallback image preparing function interface.
 */
interface ImageFactoryInterface {

  public const JPG_PATTERN = '/(\\.jpg\\.jpg|\\.jpeg\\.jpg|\\.png\\.jpg|\\.gif\\.jpg|\\.webp\\.jpg)$/';
  public const WEBP_PATTERN = '/(\\.jpg\\.webp|\\.jpeg\\.webp|\\.png.webp|\\.gif\\.webp|\\.webp\\.webp)$/';

  /**
   * Prepare jpg fallback image.
   *
   * @param string $uri
   *   The image uri.
   * @param \Drupal\Core\Image\ImageInterface $webp_image
   *   The webp image.
   * @param int $quality
   *   The quality factor of image.
   *
   * @return bool
   *   Return true if successful generated fallback image otherwise false.
   */
  public function createImageCopy(string $uri, ImageInterface $webp_image, int $quality): bool;

  /**
   * Return te source image.
   *
   * @return \Drupal\Core\Image\ImageInterface
   *   The source image.
   */
  public function getSourceImage(): ImageInterface;

  /**
   * Set the destination uri.
   *
   * @param string $uri
   *   The webp uri.
   */
  public function setDestinationUri(string $uri = ''): void;

  /**
   * Return the destination uri.
   *
   * @return string
   *   The destination uri.
   */
  public function getDestinationUri(): string;

  /**
   * Return the file mimetype.
   *
   * @return string
   *   The mimetype string.
   */
  public function getMimeType(): string;

  /**
   * Delete fallback jpg of styles.
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   The image style which flushing.
   */
  public function fileDelete(EntityInterface $file): void;

  /**
   * Return the webp to jpg replaced text.
   *
   * @param string $value
   *   The text to replace.
   *
   * @return string
   *   The replaced text.
   */
  public function getJpg(string $value): string;

  /**
   * Return the webp to jpg replaced text for one single URL.
   *
   * @param string $srcUrl
   *   The text to replace.
   *
   * @return string
   *   The replaced text.
   */
  public function getProcessedJpgUrl(string $srcUrl): string;

  /**
   * Invalidate images after file created or updated.
   *
   * @param \Drupal\Core\Entity\EntityInterface $crop
   *   The CropInterface.
   */
  public function invalidateFallbackImagesByCrop(EntityInterface $crop);

}
