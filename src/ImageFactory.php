<?php

namespace Drupal\wpf;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory as CoreImageFactory;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\file\FileInterface;
use Drupal\system\Plugin\ImageToolkit\GDToolkit;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * ImageFactory service for creating fallback image.
 */
class ImageFactory implements ImageFactoryInterface {

  use StringTranslationTrait;

  /**
   * The image factory service.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The image quality factor.
   *
   * @var int
   */
  protected $quality;

  /**
   * The original image uri.
   *
   * @var string
   */
  protected $uri;

  /**
   * The fallback image uri.
   *
   * @var string
   */
  protected $destinationUri;

  /**
   * The image toolkit.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitInterface
   */
  protected $toolkit;

  /**
   * The toolkit id definition.
   *
   * @var string
   */
  protected $toolkitId;

  /**
   * The source image.
   *
   * @var \Drupal\Core\Image\ImageInterface
   */
  protected $sourceImage;

  /**
   * The fallback image mime type.
   *
   * @var string
   */
  protected $mimeType;

  /**
   * The MimeTypeGuesserInterface definition.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * The disabled image styles.
   *
   * @var array
   */
  protected $disabledStyles;

  /**
   * Constructs an ImageFactory object.
   *
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image.factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\File\MimeType\MimeTypeGuesserInterface $mime_type_guesser
   *   The MIME type guesser service.
   */
  public function __construct(
    CoreImageFactory $image_factory,
    LoggerChannelFactoryInterface $logger,
    TranslationInterface $string_translation,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    MimeTypeGuesserInterface $mime_type_guesser
  ) {
    $this->imageFactory = $image_factory;
    $this->logger = $logger;
    $this->setStringTranslation($string_translation);
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $settings = $config_factory->get('wpf.settings');
    $this->quality = $settings->get('quality') ?? 75;
    $this->disabledStyles = $settings->get('styles.disabled') ? array_filter($settings->get('styles.disabled')) : [];
  }

  /**
   * {@inheritDoc}
   */
  public function createImageCopy(string $uri, ImageInterface $webp_image, int $quality = NULL): bool {
    $this->uri = $uri;
    $webp_uri = $webp_image->getSource();
    $this->mimeType = 'image/jpeg';
    if ($quality !== NULL) {
      $this->quality = $quality;
    }

    $this->toolkitId = $this->configFactory->get('system.image')->get('toolkit');
    // Fall back to GD if the installed imagick does not exists.
    if ($this->toolkitId === 'imagick' && !extension_loaded('imagick')) {
      $this->toolkitId = 'gd';
    }
    elseif ($this->toolkitId === 'imagick') {
      // @todo prepare function for imagick toolkit.
      $this->toolkitId = 'gd';
    }

    // Get source image.
    $this->sourceImage = $this->imageFactory->get($this->uri, $this->toolkitId);
    // Get toolkit.
    $this->toolkit = $this->sourceImage->getToolkit();
    // Set destination uri.
    $this->setDestinationUri($webp_uri);

    if ($this->sourceImage === NULL ||
      $this->toolkit === NULL ||
      $this->destinationUri === NULL) {
      // Missing data to generate image.
      return FALSE;
    }

    if ($this->toolkitId === 'imagemagick') {
      // Using imagemagick to generate fallback jpg image.
      $image = $this->createImageMagickImage($webp_uri);
      return $image !== FALSE;
    }

    // Using GD to generate fallback jpg image.
    if ($this->toolkit instanceof GDToolkit) {
      /** @var \GdImage $gd_source_image */
      $gd_source_image = $this->toolkit->getImage();
      if ($gd_source_image !== NULL && $this->destinationUri !== NULL) {
        $success = FALSE;
        if ($image = imagecreatefromwebp($webp_uri)) {
          $success = imagejpeg($image, $this->destinationUri, $this->quality);
        }
        if ($success === FALSE) {
          $error = $this->t('Could not generate fallback image.');
          $this->logger->get('wpf')->error($error);
        }
        return $success;
      }
    }

    // Can't generate a GD resource from the source image, fail safely.
    $error = $this->t('Could not generate image resource from URI @uri.',
      ['@uri' => $uri]);
    $this->logger->get('wpf')->error($error);

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setDestinationUri(string $uri = NULL): void {
    if ($uri === NULL) {
      return;
    }
    $image_style = $this->extractImageStyleFromUri($uri);
    if ($image_style !== NULL) {
      if (in_array($image_style, $this->disabledStyles)) {
        $this->destinationUri = $uri;
        return;
      }
    }
    $parts = explode('.', $uri);
    $last = array_key_last($parts);
    if ($last !== NULL && isset($parts[$last]) && $parts[$last] === 'webp') {
      // If there's no file extension before the ending .webp,
      // then the original image was WEBP and we shouldn't overwrite it.
      if (!empty($parts[$last - 1]) && !$this->isFileType($uri, $parts[$last])) {
        $parts[] = 'jpg';
      }
      else {
        $parts[$last] = 'jpg';
      }
    }

    $this->destinationUri = implode('.', $parts);
  }

  /**
   * {@inheritDoc}
   */
  public function getSourceImage(): ImageInterface {
    return $this->sourceImage;
  }

  /**
   * {@inheritDoc}
   */
  public function getDestinationUri(): string {
    return $this->destinationUri;
  }

  /**
   * {@inheritDoc}
   */
  public function getMimeType(): string {
    return $this->mimeType;
  }

  /**
   * Generate image with imagemagick toolkit.
   *
   * @param string $uri
   *   The derivative image uri.
   *
   * @return false|string
   *   The destunation uri or false.
   */
  protected function createImageMagickImage(string $uri) {
    $imageMagickImg = $this->imageFactory->get($uri, $this->toolkit->getPluginId());
    // Convert webp image to jpg.
    $imageMagickImg->apply('convert',
      ['extension' => 'jpg', 'quality' => $this->quality]);
    $imageMagickImg->getToolkit()->arguments()->setSourceFormat('webp');

    $this->setDestinationUri($uri);
    $image = $this->destinationUri;
    if ($imageMagickImg->save($image)) {
      $msg = $this->t(
        'Generated Jpg image with Image Magick. Quality: @quality Destination: @destination',
        [
          '@quality' => $this->quality,
          '@destination' => $image,
        ]
      );
      $this->logger->get('wpf')->info($msg);
    }
    else {
      $image = FALSE;
      $error = $this->t('Imagemagick issue: Could not generate WebP image.');
      $this->logger->get('wpf')->error($error);
    }

    return $image;
  }

  /**
   * {@inheritDoc}
   */
  public function fileDelete(EntityInterface $file): void {
    if (!$file instanceof FileInterface) {
      return;
    }
    $uri = $file->getFileUri();
    $styles = $this->entityTypeManager->getStorage('image_style')
      ->loadMultiple();
    /** @var \Drupal\image\Entity\Entity $style */
    foreach ($styles as $style) {
      $style_uri = $style->buildUri($uri);
      $copy_uri = $this->getJpg($style_uri);
      if (file_exists($copy_uri)) {
        $this->fileSystem->delete($copy_uri);
      }
    }
  }

  /**
   * Checks if the URI contains another image extension before the last one.
   *
   * @param string $uri
   *   File URI.
   * @param string $lastPart
   *   Last extension from the URI.
   *
   * @return bool
   *   TRUE if it contains another image extension. For example file.webp.jpg.
   *   FALSE if not.
   */
  protected function isFileType(string $uri, string $lastPart) {
    $uriWithoutLastExtension = str_replace('.' . $lastPart, '', $uri);
    /** @var \Symfony\Component\Mime\MimeTypeGuesserInterface $mime */
    $mime = $this->mimeTypeGuesser->guessMimeType($uriWithoutLastExtension);
    return (strpos($mime, 'image') !== FALSE);
  }

  /**
   * {@inheritDoc}
   */
  public function getJpg(string $value): string {
    $isSrcSet = (strpos($value, ',') !== FALSE);

    if ($isSrcSet) {
      $srcSetResults = [];
      $srcUrls = explode(',', $value);

      foreach ($srcUrls as $srcUrl) {
        $srcSetResults[] = $this->getProcessedJpgUrl($srcUrl);
      }

      return implode(', ', $srcSetResults);
    }
    else {
      return $this->getProcessedJpgUrl($value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getProcessedJpgUrl(string $srcUrl): string {
    $urlParts = explode('?', trim($srcUrl));

    if (isset($urlParts[0])) {
      $this->setDestinationUri($urlParts[0]);
      $urlParts[0] = $this->getDestinationUri();
    }

    return implode('?', $urlParts);
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateFallbackImagesByCrop(EntityInterface $crop) {
    $fileId = $crop->entity_id->value ?? NULL;
    $entity_type = $crop->getEntityTypeId();
    if (empty($fileId) || empty($entity_type)) {
      return;
    }

    $file = $this->entityTypeManager->getStorage($entity_type)->load($fileId);
    if ($file !== NULL) {
      $this->fileDelete($file);
    }
  }

  /**
   * Extracts the image style from a styled image URI.
   *
   * @param string $styledUri
   *   The URI of the styled image.
   *
   * @return string|null
   *   The extracted image style or NULL if not found.
   */
  protected function extractImageStyleFromUri($styledUri) {
    $pattern = '/\/styles\/([^\/]+)\//';
    preg_match($pattern, $styledUri, $matches);

    if (!empty($matches) && isset($matches[1])) {
      // The image style name.
      return $matches[1];
    }

    // No image style found in the URI.
    return NULL;
  }

}
