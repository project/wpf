<?php

namespace Drupal\wpf\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Webp fallback image route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach (['image.style_public', 'image.style_private'] as $route_id) {
      if ($route = $collection->get($route_id)) {
        $route->setDefault(
          '_controller', 'Drupal\wpf\Controller\ImageStyleDownloadController::deliver',
          'required_derivative_scheme', 'public'
        );
      }
    }
  }

}
