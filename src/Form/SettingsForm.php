<?php

namespace Drupal\wpf\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Webp fallback image settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wfp_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['wpf.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('wpf.settings');
    $form['quality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quality'),
      '#default_value' => $settings->get('quality'),
    ];

    $form['styles'] = [
      '#type' => 'details',
      '#title' => $this->t('Image styles'),
      '#open' => FALSE,
    ];
    $form['styles']['disabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disable fallback image for styles'),
      '#options' => image_style_options(FALSE),
      '#default_value' => $settings->get('styles.disabled') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $settings = $this->config('wpf.settings');
    $settings->set('quality', (int) $form_state->getValue('quality'))->save();
    $settings->set('styles.disabled', $form_state->getValue('disabled'))->save();

    parent::submitForm($form, $form_state);
  }

}
