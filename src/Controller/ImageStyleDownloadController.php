<?php

namespace Drupal\wpf\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\image\Controller\ImageStyleDownloadController as CoreImageStyleDownloadController;
use Drupal\image\ImageStyleInterface;
use Drupal\wpf\ImageFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Returns responses for Webp fallback image.
 */
class ImageStyleDownloadController extends CoreImageStyleDownloadController {

  /**
   * The image quality definition.
   *
   * @var int
   */
  protected $quality;

  /**
   * The ImageFactoryInterface definition.
   *
   * @var \Drupal\wpf\ImageFactoryInterface
   */
  protected $wpfImageFactory;

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->settings = $instance->config('wpf.settings');
    $instance->quality = $instance->settings->get('quality') ?? 75;
    $instance->wpfImageFactory = $container->get('wpf.image_factory');

    return $instance;
  }

  /**
   * Builds the response.
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style, string $required_derivative_scheme) {
    $disabled_styles = array_filter(($this->settings->get('styles.disabled') ?? []));
    if (in_array($image_style->id(), $disabled_styles)) {
      return parent::deliver($request, $scheme, $image_style, $required_derivative_scheme);
    }

    // Interesting just the defined in JPEG_PATTERN extensions.
    if (!preg_match(ImageFactoryInterface::JPG_PATTERN, $request->getPathInfo())) {
      // Don't match with extensions go to system image style delivery method.
      return parent::deliver($request, $scheme, $image_style, $required_derivative_scheme);
    }

    // Prepare image uri.
    $target = $request->query->get('file');
    $image_uri = $scheme . '://';
    $image_uri .= preg_replace('/\\.jpg$/', '', $target);

    // Get the image derivative uri from image style.
    $derivative_uri = $image_style->buildUri($image_uri);
    if (!preg_match('/.webp$/', $derivative_uri)) {
      // The image style is not converted to webp format go to system deliver.
      return parent::deliver($request, $scheme, $image_style, $required_derivative_scheme);
    }

    $headers = [];
    if ($scheme === 'private') {
      // Let other modules provide headers and control access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$image_uri]);
      if (in_array(-1, $headers, TRUE) || empty($headers)) {
        throw new AccessDeniedHttpException();
      }
    }

    $success = file_exists($derivative_uri);
    $lock_name = 'image_style_deliver:' . $image_style->id() . ':' . Crypt::hashBase64($image_uri);
    if ($success === FALSE) {
      $lock_acquired = $this->lock->acquire($lock_name);
      if (!$lock_acquired) {
        // Retry again in 3 seconds.
        // Currently no browsers are known to support Retry-After.
        throw new ServiceUnavailableHttpException(3, 'Image generation in progress. Try again shortly.');
      }
    }

    // Image style doesn't use webp converting don't need creating fallback.
    $success = file_exists($derivative_uri);
    if ($success === FALSE) {
      $success = $image_style->createDerivative($image_uri, $derivative_uri);
    }

    if (!empty($lock_acquired)) {
      $this->lock->release($lock_name);
    }

    if ($success) {
      // We have to prepare the image for request.
      /** @var \Drupal\Core\Image\ImageInterface $webp_image */
      $webp_image = $this->imageFactory->get($derivative_uri);
      if ($this->wpfImageFactory->createImageCopy($image_uri, $webp_image)) {
        return $this->response(
          $this->wpfImageFactory->getDestinationUri(),
          $headers, $scheme, $this->wpfImageFactory->getMimeType());
      }
    }

    // Not generated images log the warning.
    $this->logger->warning('Unable to generate the derived image located at %path.', ['%path' => $derivative_uri]);
    return new Response($this->t('Error generating image.'), 500);
  }

  /**
   * Returns a WebP image as response.
   *
   * @param string $file
   *   Path to image file.
   * @param array $headers
   *   Response headers.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   * @param string $content_type
   *   The acceptable content type.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The transferred file as response.
   */
  protected function response(string $file, array $headers, string $scheme, string $content_type): BinaryFileResponse {
    $headers = array_merge($headers, [
      'Content-Type' => $content_type,
      'Content-Length' => filesize($file),
    ]);

    return new BinaryFileResponse($file, 200, $headers, $scheme !== 'private');
  }

}
