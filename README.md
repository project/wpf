### WebP fallback image
Creates a fallback jpg image from converted webp image for old browsers
which are not able to handle the webp format.

#### Description
Use the core webp for converting in image styles.
This case prepare the best quality images for modern web browsers and
this module will prepare a fallback jpg image from webp to be used by
fewer browsers: https://caniuse.com/webp.
The module does not generate unnecessary jpg images.
More background: https://www.brainsum.com/blog/webp-right-way-and-wrong-way-drupal


#### Installation:
* Install and enable this module
* Enable responsive image module
* In image styles use Image convert to webp
* Create each image style responsive image style
* Use responsive image style in entities display settings for images

#### Dependencies:
* responsive image

#### Similar modules:
* webp
* imageapi optimize webp

#### Rationale
While similar modules provide solutions for generating the fallback
jpeg images for webp, we found that they are doing this in a sub-optimal way.
The primary problem is that they don't create the webp derivative directly
from the source file, but they are first creating a jpeg
(or sometimes png or gif according to the original file's format)
and then converting it to a webp. As the image styles are in most cases
downscaled and the jpeg lossy compression is applied, the potential quality
and/or size benefits of moving to the webp format this way are wasted.
Another issue, a performance-related one we've seen in similar modules
is that they are creating the jpeg and the webp derivatives at the same,
single request. We believe that a request for a webp image should not generate
a jpeg - maybe that jpeg will never be used! Our module will generate the jpeg
images only when they are requested - it might also create 2 images in the same
request, when the request goes for a jpeg but the webp does not exist yet, but
this is a very unlikely case.
